$(document).ready(function () {
    $('.slider_top').bxSlider({
        mode: 'fade',
        captions: true,
        controls: false,
        auto: true
    });

    $(document).ready(function ($) {
        $('#tabs').tabulous();
    });
});

$(function () {
    $('.popup3').hide();
    $('#hide-layout').css({opacity: .5});

    $('.open_link3').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 0);
        $('#hide-layout, #popup3').fadeIn(300);
        return false;
    })
    $('.popup5').hide();
    $('#hide-layout').css({opacity: .5});

    $('.open_link5').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 0);
        $('#hide-layout, #popup5').fadeIn(300);
        return false;
    })
    $('.popup6').hide();
    $('#hide-layout').css({opacity: .5});

    $('.open_link6').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 0);
        $('#hide-layout, #popup6').fadeIn(300);
        return false;
    })
    $('.popup4').hide();
    $('#hide-layout').css({opacity: .5});

    $('.open_link4').click(function () {
        $('body,html').animate({}, 0);
        $('#hide-layout, #popup4').fadeIn(300);
        return false;
    })
    $('#btn-close, #hide-layout').click(function () {
        $('#hide-layout, .popup').fadeOut(300);
    });

    var $faqItem = $('.faqItem');
    $faqItem.on('click',function(e){
        e.preventDefault();
        var $this = $(this);
        var $target = $($this.attr('href'));
        if($this.hasClass('active-link')){
            $this.removeClass('active-link');
            $target.fadeOut();
        }else{
            $this.addClass('active-link');
            $target.slideDown('slow');
        }
    });

    $(".modalbox").fancybox();

    var $productDays = $('#productDays');
    var $productForm = $('#productForm');
    var $productFormButton = $('#productFormButton');
    var $orderFormWrapper = $('#orderFormWrapper');
    $('.makeOrder').on('click',function(e){
        e.preventDefault();
        var $this = $(this);
        var days = $this.data('size');
        $productDays.val(days).trigger('change');
        $productFormButton.trigger('submit');
        $.fancybox.open($orderFormWrapper);
    });

    //$("#f_send").on("click", function () {
    //    $.post('/feedback.php', $("#f_contact").serialize(), function (data) {
    //        if (data) {
    //            $("#f_contact .err").html(data);
    //        } else {
    //            $("#f_contact").fadeOut("fast", function () {
    //                $(this).before("<p><strong>Ваше сообщение отправлено!</strong></p>");
    //                setTimeout("$.fancybox.close()", 1000);
    //            });
    //        }
    //    });
    //});




})