"use strict";
appMakeBeCool.gateway.addClass('FormAjax', function(properties, $, $window, $document) {
    //PRIVATE VARIABLES
    var _formAjax = this,
    _defaults = {
        // elements
        forms: [],
        successMessageSuffix: '_SuccessMessage',
        errorMessageSuffix: '_ErrorMessage',
        formMessage: '.formMessage',

        // prop
        // data
        // classes ans styles
    },
    _properties = $.extend(_defaults, properties),
    _globals = {
        // elements
        formMessage: null,
        forms: [],
        successMessages: [],

        // prop
        preloaded: false
    },

    //PRIVATE METHODS
    _init = function() {
        appMakeBeCool.gateway.base.Class.apply(_formAjax, [_properties]);
        if(!_globals.preloaded) {
            return _formAjax.init();
        }
        _formAjax.globals.customCreate = function() {
            _config();
            _setup();
            _setBinds();
            _setCustomMethods();
        };
        _formAjax.create();
    },

    _config = function() {
        if(_properties.forms.length) {
            for (var i in _properties.forms){
                _globals.forms.push($(_properties.forms[i]));
                _globals.successMessages.push($(_properties.forms[i]+_properties.successMessageSuffix));
            }
        }
        _globals.formMessage = $(_properties.formMessage);
    },

    _setup = function() {
        if(_globals.forms.length) {
            for (var i in _globals.forms){
                _globals.forms[i].ajaxForm({
                    dataType:  'json',
                    beforeSubmit: _formBeforeSubmit,
                    success:   _formSuccess
                });
            }
        }
    },

    _setBinds = function() {},

    _binds = function() {
        return {}
    },

    _triggers = function(){
        return {}
    },

    _setCustomMethods = function() {
        _formAjax.globals.customResurrect = function() {};
        _formAjax.globals.customDestroy = function() {};
    },

    _formBeforeSubmit = function(arr, $form, options){},

    _formSuccess = function(response, statusText, xhr, $form){
        if(response.success) {
            $form.slideUp('slow', function(){
                var formSuccessMessageId = $form.attr('id')+_properties.successMessageSuffix;
                $('#'+formSuccessMessageId).slideDown('slow',function(){
                    setTimeout(function(){
                        $.fancybox.close();
                        _resetForm($form);
                        $('#'+formSuccessMessageId).hide();
                    }, 1000);
                });
            });
        } else {
            $form.slideUp('slow', function(){
                var formErrorMessageId = $form.attr('id')+_properties.errorMessageSuffix;
                $('#'+formErrorMessageId).slideDown('slow',function(){
                    setTimeout(function(){
                        $.fancybox.close();
                        _resetForm($form);
                        $('#'+formErrorMessageId).hide();
                    }, 1000);
                });
            });
        }

    },
    _resetForm = function($form){
        $form.show();
    };

    //PUBLIC METHODS
    _formAjax.addMethod('init', function() {
        _formAjax.bind($window, _formAjax.globals.classType+'_Init', function(e, data, el) {
            _globals.preloaded = true;
            _init();
        });
    });

    //GO!
    _init();
});