"use strict";
appMakeBeCool.gateway.addClass('FormValidate', function(properties, $, $window, $document) {
    //PRIVATE VARIABLES
    var _formValidate = this,
    _defaults = {
        // elements
        forms: []

        // prop
        // data
        // classes ans styles
    },
    _properties = $.extend(_defaults, properties),
    _globals = {
        // elements
        forms: [],

        // prop
        preloaded: false
    },

    //PRIVATE METHODS
    _init = function() {
        appMakeBeCool.gateway.base.Class.apply(_formValidate, [_properties]);
        if(!_globals.preloaded) {
            return _formValidate.init();
        }
        _formValidate.globals.customCreate = function() {
            _config();
            _setup();
            _setBinds();
            _setCustomMethods();
        };
        _formValidate.create();
    },

    _config = function() {
        if(_properties.forms.length) {
            for (var i in _properties.forms){
                _globals.forms.push($(_properties.forms[i]));
            }
        }
    },

    _setup = function() {
        if(_globals.forms.length) {
            for (var i in _globals.forms){
                _globals.forms[i].validate();
            }
        }
    },

    _setBinds = function() {},

    _binds = function() {
        return {}
    },

    _triggers = function(){
        return {}
    },

    _setCustomMethods = function() {
        _formValidate.globals.customResurrect = function() {};
        _formValidate.globals.customDestroy = function() {};
    };

    //PUBLIC METHODS
    _formValidate.addMethod('init', function() {
        _formValidate.bind($window, _formValidate.globals.classType+'_Init', function(e, data, el) {
            _globals.preloaded = true;
            _init();
        });
    });

    //GO!
    _init();
});