<?php
$properties =& $scriptProperties;

$properties['id'] = isset($properties['id']) ? (int)$properties['id'] : $modx->resource->get('id');
$properties['page'] = $_REQUEST['page'];

$output = array();

$cacheOptions = array(
    xPDO::OPT_CACHE_KEY => 'baseThemeCache',
);

$cacheKey = 'jsonResourceInfo/'.md5(serialize($properties));

if($modx->getCacheManager() && is_null($output = $modx->cacheManager->get($cacheKey, $cacheOptions))) {

    $urlAttr = array();
    if((integer)$_REQUEST['page']){
        $urlAttr['page'] = (integer)$_REQUEST['page'];
    }
    $resource = $modx->resource;
    if($properties['id'] != $modx->resource->get('id')) {
        $resource = $modx->getObject('modResource',$properties['id']);
    }
    $output = array(
        'pagetitle' => $resource->get('pagetitle'),
        'longtitle' => $resource->get('longtitle'),
        'description' => $resource->get('description'),
        'url' => $modx->makeUrl($resource->get('id'), '', $urlAttr),
    );
    
    $modx->cacheManager->set($cacheKey, $output, 0, $cacheOptions);
}

return json_encode($output);