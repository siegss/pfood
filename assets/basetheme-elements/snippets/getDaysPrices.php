<?php
$output = '';
if(!$prices = $properties['msoptionsprice']) return $output;
$pdoTools = $modx->getService('pdoTools');

foreach($prices as $key => $price){
    $data = [
        'days' => $key,
        'price' => $price,
        'category_name' => $categoryName,
        'price_per_day' => ($key > 1) ? round($price / $key, 1) : ''
    ];
    $output .= $pdoTools->getChunk($tpl,$data);
}
return $output;