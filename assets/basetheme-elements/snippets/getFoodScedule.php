<?php
$output = ['success' => false];
if(empty($schedule) && empty($modx->resource->weekScedule[1])) return $modx->toJSON($output);
$pdoTools = $modx->getService('pdoTools');
$weekSchedule = $modx->fromJSON($schedule);
$links = '';
$tabs = '';
foreach($weekSchedule as $key => $day){
    $tab = '';
    $linkData = [
        'day' => $day['day'],
        'number' => $key+1
    ];
    $links .= $pdoTools->getChunk('@INLINE <li><a href="#week-day-{$number}" title="">{$day}</a></li>',$linkData);
    $daySchedule = $modx->fromJSON($day['dayScedule']);
    foreach($daySchedule as $meal){
        $tab .= $pdoTools->getChunk('mealItemProduct',$meal);
    }
    $tabs .= $pdoTools->getChunk('@INLINE <div id="week-day-{$number}">{$tab_content}</div>',['number' => $key + 1,'tab_content' => $tab]);
}
$output= [
    'success' => true,
    'links' => $links,
    'tabs' => $tabs
];
return $modx->toJSON($output);