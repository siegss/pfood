<?php
$properties =& $scriptProperties;

$properties['fields'] = ['name','email','phone'];
$properties['emailTo'] = !empty($properties['emailTo']) ? $properties['emailTo'] : $modx->getOption('emailsender');;
$properties['subject'] = !empty($properties['subject']) ? $properties['subject'] : 'Contact Form';
$properties['tpl'] = !empty($properties['tpl']) ? $properties['tpl'] : 'emailContacts';

$fields = array();
foreach($properties['fields'] as $fieldName) {
    $fields[$fieldName] = isset($_POST[$fieldName]) ? $_POST[$fieldName] : null;
}

$errors = array();
$errorCheck = false;

$output = array('success'=>true);

if($errorCheck) {
    $output = array(
        'success'   =>  false,
        'errors'    =>  $errors
    );
} else {

    if(!is_null($properties['emailTo'])) {
        $message = $modx->getChunk($properties['tpl'], $fields);

        $modx->getService('mail', 'mail.modPHPMailer');
        $modx->mail->set(modMail::MAIL_BODY,$message);
        $modx->mail->set(modMail::MAIL_FROM,$modx->getOption('emailsender'));
        $modx->mail->set(modMail::MAIL_FROM_NAME,$modx->getOption('site_name'));
        $modx->mail->set(modMail::MAIL_SENDER,$modx->getOption('site_name'));
        $modx->mail->set(modMail::MAIL_SUBJECT,$properties['subject']);
        $modx->mail->address('to',$properties['emailTo']);
        $modx->mail->setHTML(true);
        if (!$modx->mail->send()) {
            $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$err);
        }
        $modx->mail->reset();
    }
}

return json_encode($output);