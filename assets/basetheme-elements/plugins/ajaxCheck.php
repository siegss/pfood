<?php
$eventName = $modx->event->name;
switch($eventName) {
    case 'OnLoadWebDocument':
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $modx->setPlaceholder('ajax',1);
        } else {
            $modx->setPlaceholder('ajax',0);
        }
        break;
}