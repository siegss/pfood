<div class="article">
    <div class="leftblock">
        <img src="{$img}">
    </div>
    <div class="rightblock">
        <div class="sorcenews">
            <span>{$pagetitle}</span>
            <span>{$intorotext}</span>
        </div>
        <span class="titlenews">{$longtitle ?: $pagetitle}</span>
        <div class="allnews">
           {$content}
        </div>
        <span class="datenews">{$_modx->lexicon('lf_publication_data')} {$publishedon|date_format:"%d.%m.%Y"}</span>
        {if $textfield ?}
        <div class="morebtn">
            <a href="{$textfield}" rel="nofollow">{$_modx->lexicon('lf_read_more')}</a>
        </div>
        {/if}
    </div>
</div>