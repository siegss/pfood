<div class="item{($idx % 2 == 0) ? '2' : ''}" data-d="{$idx % 2}" id="bx_{$id}">
    <div class="cont">
        <div class="float">
            <div class="photo"><img src="{$img}"/></div>
        </div>
        <div class="info">
            <div class="rev"></div>
            <div class="title"><b>{$pagetitle}</b> - {$introtext}
                {if $textfield ?}
                <a href="{$textfield}"><img src="{$_modx->config['basetheme.design_url']}images/instagram.png"/></a>
                {/if}
            </div>
            <div class="text">{$content}</div>
        </div>
        <br clear="both"/>
    </div>
</div>