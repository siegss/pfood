<div class="cont">
    <div class="about_us_top">
        <h1>{$_modx->resource.pagetitle}</h1>
        {$_modx->resource.content}
    </div>
    <div class="why_us">
        <h1>{$_modx->lexicon('lf_why_we')}</h1>
        <div>
            {$_modx->runSnippet('!getMigxItems',[
            'tvname' => 'gallery',
            'tpl' => 'tesisItemAbout',
            'limit' => 6
            ])}
        </div>
    </div>
</div>
<!-- End cont -->