<div class="cont">
    <h1 class="greenh1">{$_modx->resource.pagetitle}</h1>
</div><!-- End Cont -->

<div class="reviews">
    {$_modx->runSnippet('pdoResources',[
    'parents' => $_modx->config['basetheme.id_reviews'],
    'depth' => 0,
    'includeTVs' => 'img,textfield',
    'tvPrefix' => '',
    'includeContent' => 1,
    'tpl' => 'reviewItemAbout'
    ])}
    <br/>
</div>