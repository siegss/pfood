<div class="cont slidercont">
    <div class="slidermain">
        <div class="slider-wrapper">
            <ul class="slider_top">
                {$_modx->runSnippet('pdoResources',[
                    'parents' => $_modx->config['site_start'],
                    'depth' => 0,
                    'sortby' => 'menuindex',
                    'sortdir' => 'ASC',
                    'includeContent' => 1,
                    'tpl' => '@INLINE <li>{$content}</li>'
                ])}
            </ul>
        </div>
    </div>
</div>

<div class="cont maincont">
    <div class="slider_top_stat">
        {$_modx->lexicon('lf_food_delivery_in')}
        <img width="331" src="/assets/basetheme-design/images/depositphotos_23900171_original.jpg" height="221">
    </div>
</div><!-- cont END -->

<div class="hr_index">
</div>

<div class="cont">
    <center>
        <div class="programms">
            {$_modx->runSnippet('pdoResources@middleMenu',[
                'parents' => $_modx->config['basetheme.id_nutrition_programs'],
                'includeTVs' => 'img',
                'tvPrefix' => '',
                'tpl' => 'nutritionProgramsItemIndex'
            ])}
            <!--<div class="hr"></div>-->
            <div class="clear"></div>
        </div>
    </center>
</div>
<div class="podbor">
    <div class="cont">
        <table width="100%">
            <tbody>
            <tr valign="top">
                <td>
                    <div class="greentitle">{$_modx->lexicon('lf_program_select')}</div>
                    <p>{$_modx->resource.text}</p>
                </td>
                <td align="right">
                    <div class="items">
                        {$_modx->runSnippet('!getMigxItems',[
                            'tvname' => 'gallery',
                            'tpl' => 'programSelectItemIndex',
                            'limit' => 6
                        ])}
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="cont">
    <div class="other_content_index">
        {$_modx->runSnippet('!getMigxItems',[
        'tvname' => 'linkGallery',
        'tpl' => 'linkGalleryItemIndex',
        'limit' => 3
        ])}
        <br clear="both">
    </div>
</div><!-- End cont -->
<br>