<div class="cont">
    <h1>{$_modx->resource.pagetitle}</h1>
    <br><br>
    {$_modx->runSnippet('pdoResources',[
        'parents' => $_modx->config['basetheme.id_smi'],
        'depth' => 0,
        'includeTVs' => 'img,textfield',
        'tvPrefix' => '',
        'includeContent' => 1,
        'tpl' => 'smiItemAbout',
    ])}
</div>
