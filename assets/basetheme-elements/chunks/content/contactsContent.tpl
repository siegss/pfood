<div class="cont">
	<img src="{$_modx->resource.img}" class="contact-img">
	<h1>Контакты </h1>
	<table class="cont-info">
		<tr>
			<td>{$_modx->lexicon('lf_reference_service')} </td>
			<td>{$_modx->lexicon('lf_phone_1')}</td>
		</tr>
		<tr>
			<td> </td>
			<td>{$_modx->lexicon('lf_phone_2')}</td>
		</tr>
		<tr>
			<td>{$_modx->lexicon('lf_contact_email')} </td>
			<td>{$_modx->config['emailsender']} </td>
		</tr>
		<tr>
			<td>{$_modx->lexicon('lf_hours_of_operation')}</td>
			<td>{$_modx->lexicon('lf_hours_of_operation_value')}</td>
		</tr>
	</table>
	<br>
	<div class="kod">
		{$_modx->lexicon('lf_ogrn')} {$_modx->lexicon('lf_ogrn_value')}<br>
		{$_modx->lexicon('lf_inn')} {$_modx->lexicon('lf_inn_value')}<br>
		{$_modx->lexicon('lf_kpp')} {$_modx->lexicon('lf_kpp_value')}
	</div><br>
	<br><br>
	<h2>{$_modx->lexicon('lf_faq')}</h2>

	<div class="questions">
		{$_modx->runSnippet('!getMigxItems',[
		'tvname' => 'linkGallery',
		'tpl' => 'faqItemContacts',
		])}
	</div>
</div><!-- End cont -->
