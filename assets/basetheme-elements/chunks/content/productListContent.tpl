<div class="cont">
    <h1><span class="zag">{$_modx->resource.pagetitle}</span></h1>

    <div class="left_cat">
        <div class="title">
            {$_modx->resource.pagetitle}
        </div>
        {$_modx->getChunk('productCategoriesCommon')}
    </div>
    <div class="cat">
        <h1>{$_modx->resource.pagetitle}</h1>
        {$_modx->runSnippet('msProducts',[
            'parents' => $_modx->resource.id,
            'depth' => 0,
            'includeTVs' => 'kkalories,proteins,carbohydrates,fats,gallery',
            'tvPrefix' => '',
            'tpl' => 'productItemProduct',
        ])}
    </div>
</div>