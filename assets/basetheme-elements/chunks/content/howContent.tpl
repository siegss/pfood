<div class="cont">
    <div class="how_we_work">
        <h1>{$_modx->resource.pagetitle}</h1>

        <div class="howwork">
            {$_modx->runSnippet('!getMigxItems',[
            'tvname' => 'gallery',
            'tpl' => 'howWeWorkItemAbout',
            'limit' => 3
            ])}
        </div><!-- div END -->

    </div>
</div><!-- cont END -->

<div class="hr_index">
</div>

<div class="cont">
    <div class="what">
        <div class="row_left">
            <h3>Что такое Performance food?</h3>
            <div class="how-img">
                <a href="#popup5" class="open_link5"><img
                            src="{$_modx->config['basetheme.design_url']}images/playvideo.png"></a>
                <img src="{$_modx->resource.img}" class="first">
                <img src="{$_modx->resource.ico}" class="last">
            </div>
            <div id="popup5" class="popup">
                <span id="btn-close"></span>
                <iframe width="560" height="315" src="{$_modx->resource.textfield2}" frameborder="0" allowfullscreen>
                </iframe>
            </div>
            <p class="what-info">{$_modx->lexicon('lf_learn_about_food')}</p>
            {$_modx->runSnippet('!getMigxItems',[
            'tvname' => 'gallery',
            'docid' => $_modx->config['site_start'],
            'value' => $jsonoutput,
            'tpl' => 'programSelectItemIndex',
            'limit' => 6,
            'toJsonPlaceholder' => 'programItems'
            ])}
            {set $programItems = $modx->fromJson($_modx->getPlaceholder('programItems'))}
            <table class="about-icon">
                <tr>
                    <td><img src="{$programItems[0]['image']}">
                        <span>{$programItems[0]['title']}</span>
                        {$programItems[0]['textarea']}
                    </td>
                    <td><img src="{$programItems[3]['image']}">
                        <span>{$programItems[3]['title']}</span>
                        {$programItems[3]['textarea']}
                    </td>
                </tr>
                <tr>
                    <td><img src="{$programItems[1]['image']}">
                        <span>{$programItems[1]['title']}</span>
                        {$programItems[1]['textarea']}
                    </td>
                    <td><img src="{$programItems[4]['image']}">
                        <span>{$programItems[4]['title']}</span>
                        {$programItems[4]['textarea']}
                    </td>
                </tr>
                <tr>
                    <td><img src="{$programItems[2]['image']}">
                        <span>{$programItems[2]['title']}</span>
                        {$programItems[2]['textarea']}
                    </td>
                    <td><img src="{$programItems[5]['image']}">
                        <span>{$programItems[5]['title']}</span>
                        {$programItems[5]['textarea']}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div><!-- cont END -->

<div class="row-order">
    <div class="cont">
        <h4>{$_modx->lexicon('lf_order_and_delivery')}</h4>
        <div class="cont-order">
        </div>
    </div><!-- cont END -->
</div>