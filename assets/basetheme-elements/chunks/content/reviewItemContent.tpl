<div class="cont slidercont">
    <div class="slidermain">


        <div class="slider-wrapper">
            <ul class="slider_top">
                <li>
                    <table width="100%" style="height:465px;">
                        <tbody>
                        <tr valign="top">
                            <td class="info">
                                <div class="title"><span>Программы питания для</span><br> <span>СНИЖЕНИЯ ВЕСА</span>
                                </div>
                                <div class="hr1"><img src="/assets/basetheme-design/images/slider1min.png"
                                                      style="width: 258px;margin-top: 40px;"></div>
                                <div class="more">
                                    <span class="prognum">6 разных программ</span><br> <span class="lighttxt">для эффекивного снижения вашего веса</span><br>
                                    <span class="pricetxt">от <span class="bigprice">1917</span> руб/день</span>
                                </div>
                                <a class="sb_slaid" href="/snizhenie-vesa/">Узнать больше</a>

                            </td>
                            <td class="imagetop">
                                <img src="/assets/basetheme-design/images/slider1.png">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li>
                    <table width="100%" style="height:465px;">
                        <tbody>
                        <tr valign="top">
                            <td class="info">
                                <div class="title-blue"><span class="violet">СПОРТИВНОЕ</span><br><span>питание</span>
                                </div>

                                <div class="more moreleft moreviolet">
                                    <span class="lighttxt">20 программ питания в режиме Gold и Silver, обеспечивающих:</span><br>
                                    <span class="prognum"><img
                                                src="/assets/basetheme-design/images/sporticon1.png">от 5 до 10 приемов пищи</span> <br>
                                    <span class="prognum"><img src="/assets/basetheme-design/images/sporticon2.png">от 1500 до 6000 ККал </span>
                                    <br>
                                    <span class="pricetxt priceviolet"><img src="/assets/basetheme-design/images/slider7min.png"
                                                                            class="violetimg">от <span
                                                class="bigprice violet">815</span> руб/день</span>
                                </div>
                                <a class="sb_slaid violet-bg" href="/sport-pitanie-silver/">Узнать больше</a>

                            </td>
                            <td class="imagemidle">
                                <img src="/assets/basetheme-design/images/slider6.png">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li>
                    <table width="100%" style="height:465px;background-image: url('/assets/basetheme-design/images/slider7bg.png');">
                        <tbody>
                        <tr valign="top">
                            <td class="info">
                                <div class="title-blue" style="width: 734px;"><span
                                            class="green">ПОДАРОЧНЫЙ СЕРТИФИКАТ</span><br><span
                                            style="margin-top:11px;display:block;">Performance Food</span></div>
                                <div class="hr1"><img src="/assets/basetheme-design/images/slider7min1.png" style="margin-top: 25px;"></div>
                                <div class="more">
							<span>Закажите сертификат на <b>любой</b><br><b>рацион</b> из подходящей<br>программы питания.<br><br>Сделайте близким людям<br>
прекрасный подарок!</span><br>
                                </div>
                                <a href="/podarochnyy-sertifikat/" rel="nofollow" class="sb_slaid modalbox">Узнать
                                    больше</a>

                            </td>
                            <td class="image" style="margin-top: 75px; margin-right: 25%;">
                                <img src="/assets/basetheme-design/images/slider7.png" style="max-width:none !important;">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li>
                    <table width="100%" style="height:465px;position:relative;">
                        <tbody>
                        <tr valign="top">
                            <td class="info">
                                <div class="title"><span>Снижение веса</span><br> <span
                                            class="craplack">ПО ДИЕТЕ ДЮКАНА</span></div>
                                <div class="hr1"><img src="/assets/basetheme-design/images/slider2min.png"
                                                      style="width: 258px;margin-top:40px;"></div>
                                <div class="more moremin">
                                    <span class="prognum">4 программы белковой диеты</span><br> <span class="lighttxt">по самой популярной методике в мире</span><br>
                                    <span class="pricetxt">от <span
                                                class="bigprice craplack">1917</span> руб/день</span>
                                </div>
                                <a class="sb_slaid craplack-bg" href="/dieta-djukana/">Узнать больше</a>

                            </td>
                            <td class="image" style="position:absolute;right:0;">
                                <img src="/assets/basetheme-design/images/sldr2.png">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li>
                    <table width="100%" style="height:465px;">
                        <tbody>
                        <tr valign="top">
                            <td class="info">
                                <div class="title"><span>Программы питания</span><br> <span class="ohra">ПРИ ЗАБОЛЕВАНИЯХ</span>
                                </div>
                                <div class="hr1"><img src="/assets/basetheme-design/images/sider3min.png"
                                                      style="width: 258px;margin-top: 40px;"></div>
                                <div class="more">
                                    <span class="lighttxt">Питание при диабете, гастрите, панкреатите,<br>гипертонии, сердечных заболеваниях,<br>питание без глютена.</span><br>
                                    <span class="pricetxt">от <span class="bigprice ohra">1670</span> руб/день</span>
                                </div>
                                <a class="sb_slaid ohra-bg" href="/pitanie-bolezni/">Узнать больше</a>

                            </td>
                            <td class="image" style="margin-top: 35px;">
                                <img src="/assets/basetheme-design/images/slider3.png">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>

                <li>
                    <table width="100%" style="height:465px;">
                        <tbody>
                        <tr valign="top">
                            <td class="info">
                                <div class="title-blue"><span class="blue">СБАЛАНСИРОВАННАЯ</span><br><span>программа питания</span>
                                </div>
                                <div class="hr1"><img src="/assets/basetheme-design/images/slider4min.png" style="margin-top: 40px;"></div>
                                <div class="more">
                                    <span class="lighttxt">Для поддержания стабильного веса <br> и хорошего самочувствия</span><br>
                                    <span class="pricetxt">от <span class="bigprice blue">2127</span> руб/день</span>
                                </div>
                                <a class="sb_slaid blue-bg" href="/sbalansirovannoe-pitanie/">Узнать больше</a>

                            </td>
                            <td class="image">
                                <img src="/assets/basetheme-design/images/slider4.png">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li>
                    <table width="100%" style="height:465px;">
                        <tbody>
                        <tr valign="top">
                            <td class="info">
                                <div class="title-turq"><span class="turq">ИНДИВИДУАЛЬНОЕ ПИТАНИЕ</span></div>

                                <div class="more moreleft">
                            <span class="lighttxt">Полная индивидуальная разработка меню на основе любых<br>пожеланий клиента - предпочтений в питании, количеств приемов<br>пищи, любимых продуктов, их сочетания, соотношение белков,<br>
жиров, углеводов, заданной калорийности.</span><br>
                                    <span class="pricetxt">от <span class="bigprice turq">2500</span> руб/день</span>
                                </div>
                                <a class="sb_slaid turq-bg" href="/individual-pitanie/">Узнать больше</a>

                            </td>
                            <td class="imagebottom">
                                <img src="/assets/basetheme-design/images/slider5.png">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>

                <!--
                       <li>
                           <table width="100%" style="background-image: url('/img/slideduk.png');height:465px;">
                               <tbody>
                                   <tr valign="top">
                                   <td class="info">
                                       <div class="title"><span>Снижение веса</span><br> <span class="craplack">ПО ДИЕТЕ ДЮКАНА</span></div>
                                       <div class="hr1"><img src="img/slideduk-min.png"></div>
                                       <div class="more moremin">
                                           <span class="prognum">4 программы</span> <span class="lighttxt">белковой диеты <br> по самой популярной методике в мире</span><br>
                                           <span class="pricetxt">от <span class="bigprice craplack">1917</span> руб/день</span>
                                       </div>
                                       <a class="sb_slaid craplack-bg" href="">Узнать больше</a>

                                   </td>
                                       <td class="image">

                                   </td>
                               </tr>
                           </tbody></table>
                       </li>
                       <li>
                           <table width="100%" style="background-image: url('/img/slidebalance.png');height:465px;">
                               <tbody>
                                   <tr valign="top">
                                   <td class="info">
                                       <div class="title-blue"><span class="blue">СБАЛАНСИРОВАННАЯ</span><br><span>программа питания</span></div>
                                       <div class="hr1"><img src="img/slidebalance1.png"></div>
                                       <div class="more moremin">
                                           <span class="lighttxt">Для поддержания стабильного веса <br> и хорошего самочувствия</span><br>
                                           <span class="pricetxt">от <span class="bigprice blue">2127</span> руб/день</span>
                                       </div>
                                       <a class="sb_slaid blue-bg" href="">Узнать больше</a>

                                   </td>
                                       <td class="image">

                                   </td>
                               </tr>
                           </tbody></table>
                       </li>
                   -->
            </ul>
        </div>

    </div>
</div>

<div class="cont maincont">
    <div class="slider_top_stat">
        ДОСТАВКА ДИЕТИЧЕСКОЙ<br>
        ЕДЫ В МОСКВЕ<br>
        <img width="331" src="/assets/basetheme-design/images/depositphotos_23900171_original.jpg" height="221">
    </div>
</div><!-- cont END -->

<div class="hr_index">
</div>

<div class="cont">
    <center>
        <div class="programms">
            <div class="item">
                <a href="/snizhenie-vesa/"><img src="/assets/basetheme-design/images/a470a61f53b250e203ec585ad070cd42.png"/></a><br/>
                <a href="/snizhenie-vesa/" class="b">Снижение веса</a>
            </div>
            <div class="item">
                <a href="/sport-pitanie-silver/"><img
                            src="/assets/basetheme-design/images/523d354c77b46ebf8eee60f36e018776.png"/></a><br/>
                <a href="/sport-pitanie-silver/" class="b">Спортивное питание Silver</a>
            </div>
            <!--<div class="hr"></div>-->
            <div class="item">
                <a href="/sbalansirovannoe-pitanie/"><img
                            src="/assets/basetheme-design/images/5f78a1eca7fe3d527b3975811931050f.png"/></a><br/>
                <a href="/sbalansirovannoe-pitanie/" class="b">Сбалансированное питание</a>
            </div>
            <!--<div class="hr"></div>-->
            <div class="item">
                <a href="/individual-pitanie/"><img
                            src="/assets/basetheme-design/images/ba1407ce3d2b65763efe4c3f72baa04b.png"/></a><br/>
                <a href="/individual-pitanie/" class="b">Индивидуальное питание</a>
            </div>
            <div class="item">
                <a href="/pitanie-bolezni/"><img
                            src="/assets/basetheme-design/images/5802401c8b5f26000dfa34263bc06b03.png"/></a><br/>
                <a href="/pitanie-bolezni/" class="b">Питание при заболеваниях</a>
            </div>
            <!--<div class="hr"></div>-->
            <div class="clear"></div>
        </div>
    </center>
</div>
<div class="podbor">
    <div class="cont">
        <table width="100%">
            <tbody>
            <tr valign="top">
                <td>
                    <div class="greentitle">
                        Подбор программы питания
                    </div>
                    <p>
                        Программа питания — это рассчитанный по дням план Ваших завтраков, обедов, ужинов и даже
                        небольших «перекусов». Набор готовых блюд на весь день доставляется ежедневно, в удобное для Вас
                        время в офис или домой.
                    </p>

                </td>
                <td align="right">
                    <div class="items">
                        <div class="item">
                            <img src="/assets/basetheme-design/images/podbor1.png"> <b>Контролируемые порции</b>
                            и время приема пищи
                        </div>
                        <div class="item">
                            <img src="/assets/basetheme-design/images/podbor2.png"> <b>Гарантия</b>
                            результата
                        </div>
                        <div class="item">
                            <img src="/assets/basetheme-design/images/podbor3.png"> <b>Доставка в удобное</b>
                            место и время
                        </div>
                        <div class="item">
                            <img src="/assets/basetheme-design/images/podbor4.png"> <b>Свежеприготовленные</b>
                            блюда
                        </div>
                        <div class="item">
                            <img src="/assets/basetheme-design/images/podbor5.png"> <b>Без химических добавок</b>
                            и консервантов
                        </div>
                        <div class="item">
                            <img src="/assets/basetheme-design/images/podbor6.png"> <b>Продуманный рацион,</b>
                            разнообразное меню
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="cont">
    <div class="other_content_index">
        <div class="item1">
            <a href="/otzyvy/">Отзывы</a><br>
            <p>
                Денис Гусев: Привозят свежую еду каждый день, и никаких проблем с готовкой
            </p>
        </div>
        <div class="item2">
            <a href="/smi-o-nas/">СМИ о нас</a><br>
            <p>
                Сушка тела: тестируем диету от Performance Food
            </p>
        </div>
        <div class="item3">
            <a href="/podarochnyy-sertifikat/">Подарочный сертификат</a><br>
            <p>
                Сделайте близким людям прекрасный подарок
            </p>
        </div>
        <br clear="both">
    </div>
</div><!-- End cont -->
<br>