{set $categoryName = $_modx->runSnippet('pdoField',['id' => $_modx->resource.parent])}
<div class="cont">
    <div class="left_cat">
        {$_modx->getChunk('productCategoriesCommon')}
    </div>
    <div class="cat">
        <div class="cat_row cart">
            <div class="row_title">
                <a href="javascript://">{$categoryName} - {$_modx->resource.pagetitle}</a>
            </div>

            <div class="row_img">
                <a href="javascript://"><img src="{$image}"/></a>
            </div>

            <div class="row_info">
                <div class="info_1">
                    <span>{$_modx->lexicon('lf_average_day')}</span>
                    <ul>
                        <li><span>{$_modx->resource.kkalories}</span>{$_modx->lexicon('lf_kkals')}</li>
                        <li><span>{$_modx->resource.proteins}</span>{$_modx->lexicon('lf_proteins')}</li>
                        <li><span>{$_modx->resource.carbohydrates}</span>{$_modx->lexicon('lf_carbohydrates')}</li>
                        <li><span>{$_modx->resource.fats}</span>{$_modx->lexicon('lf_fats')}</li>
                    </ul>
                </div>

                <div class="info_2">
                    <ul>
                        {$_modx->runSnippet('!getMigxItems',[
                        'tvname' => 'gallery',
                        'tpl' => '@CODE: <li>[[+textarea]]</li>',
                        'limit' => 3
                        ])}
                    </ul>
                </div>

            </div>

            <div class="row_info_2">
                <div class="info_3">
                    {$_modx->lexicon('lf_program_description')}<br>
                    {$_modx->resource.introtext}
                </div>
                <div class="info_3">
                    {$_modx->lexicon('lf_program_includes')}<br/>
                    <br/>
                    {$_modx->resource.content}
                </div>

                <div class="info_4">
                    <a name="show_price"></a>
                    <span>{$_modx->lexicon('lf_prices')}</span>
                    {$_modx->runSnippet('getDaysPrices',[
                    'properties' => $_modx->resource.properties,
                    'tpl' => 'priceItemProduct',
                    'categoryName' => $categoryName
                    ])}

                    <small>{$_modx->lexicon('lf_call_for_individual')}</small>
                    <div id="msProduct" style="display: none">
                        <form class="ms2_form" id="productForm" method="post">
                            <input type="hidden" name="id" value="{$_modx->resource.id}"/>
                            {$_modx->runSnippet('!msOptions',[
                            'name' => 'size',
                            'tplOuter' => '@INLINE <select name="options[{$name}]" id="productDays">{$rows}</select>'
                            ])}
                            <input type="number" name="count" id="product_price" class="input-sm form-control"
                                   value="1"/>
                            <button type="submit" name="ms2_action" value="cart/add" id="productFormButton"></button>
                        </form>
                    </div>
                </div>
                <div class="info_5">
                    <span class="title">{$_modx->lexicon('lf_approximate_menu')}</span>
                    {*[[!getFoodScedule? &schedule=`[[*weekScedule]]`]]*}
                    {set $scedule = $modx->fromJSON($_modx->runSnippet('getFoodScedule',['schedule' => $_modx->resource.weekScedule]))}
                    <div id="tabs">
                        <ul>
                            {$scedule.links}
                        </ul>

                        <div id="tabs_container">
                            {$scedule.tabs}
                        </div><!--End tabs container-->

                    </div><!--End tabs-->
                </div><!--End info_5-->
            </div>


        </div>


    </div>
</div>