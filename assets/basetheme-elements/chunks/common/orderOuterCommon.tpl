<div id="formTitleBox">
    <h2>{$_modx->lexicon('lf_fill_form')}</h2>
    <h3 id="contactFormTitle">{$_modx->lexicon('lf_to_order_product')}</h3>
</div>
<form id="msOrder" class="ms2_form" action="" method="post">
    <input type="text" id="receiver" placeholder="{$_modx->lexicon('lf_your_name')}" name="receiver" value="{$receiver}" required>
    <input type="text" id="email" placeholder="{$_modx->lexicon('lf_your_email')}" name="email" value="{$email}" data-rule-required="false" data-rule-email="true">
    <input type="text" id="phone" placeholder="{$_modx->lexicon('lf_your_phone')}" name="phone" value="{$phone}" required>
    <div style="display: none">
        <div id="payments">[[+payments]]</div>
        <div id="deliveries">[[+deliveries]]</div>
    </div>
    <button type="submit" name="ms2_action" value="order/submit">{$_modx->lexicon('lf_to_order')}</button>
</form>
<h3 id="msOrder_SuccessMessage" class="formMessage" style="display: none">{$_modx->lexicon('lf_order_successMessage')}</h3>
<h3 id="msOrder_ErrorMessage" style="display: none">{$_modx->lexicon('lf_order_errorMessage')}</h3>