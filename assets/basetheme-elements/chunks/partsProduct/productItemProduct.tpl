
{set $url = $_modx->makeUrl($id,'','','full')}
<div class="cat_row">
    <div class="row_title">
        <a href="{$url}">{$_modx->resource.pagetitle}- {$pagetitle}</a>
    </div>
    <div class="row_img">
        <a href="{$_modx->makeUrl($id,'','','full')}"><img
                    src="{$image}"/></a>
    </div>
    <div class="row_info">
        {if !empty($kkalories) && !empty($proteins) && !empty($carbohydrates) && !empty($fats)}
        <div class="info_1">
            {$_modx->lexicon('lf_average_day')}
            <ul>
                {if $kkalories?}<li><span>{$kkalories}</span>{$_modx->lexicon('lf_kkals')}</li>{/if}
                {if $proteins?}<li><span>{$proteins}</span>{$_modx->lexicon('lf_proteins')}</li>{/if}
                {if $carbohydrates?}<li><span>{$carbohydrates}</span>{$_modx->lexicon('lf_carbohydrates')}</li>{/if}
                {if $fats}<li><span>{$fats}</span>{$_modx->lexicon('lf_fats')}</li>{/if}
            </ul>
        </div>
        {/if}
        <div class="info_2">
            <ul>
                {$_modx->runSnippet('!getMigxItems',[
                'tvname' => 'gallery',
                'docid' => $id,
                'tpl' => '@CODE: <li>[[+textarea]]</li>',
                'limit' => 3
                ])}
            </ul>
        </div>
        <div class="info_3">
            {$_modx->lexicon('lf_program_description')}<br>
            {$introtext}
        </div>
        <div class="info_4">
            <a href="{$url}" rel="nofollow" class="sb modalbox">{$_modx->lexicon('lf_more')}</a> <a
                    href="{$url}#show_price" class="sb_buy">{$_modx->lexicon('lf_buy')}</a>
            <div class="price">
                {$_modx->lexicon('lf_price_from',['price' => $price])}
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</div>