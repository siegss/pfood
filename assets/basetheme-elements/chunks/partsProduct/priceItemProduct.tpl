{set $daysCase = $_modx->runSnippet('units', [
    'input' => $days,
    'options' => 'день|дня|дней'
])}
<div class="price_table">
    <div>{$categoryName} - {$_modx->resource.pagetitle} - {$days} {$daysCase} </div>
    <div>
        {$price} {$_modx->lexicon('lf_currency')} {if $price_per_day?}({$_modx->lexicon('lf_price_id_day',['price' => $price_per_day])}){/if}
    </div>
    <div>
        <a href="#" data-id="{$_modx->resource.id}" data-size="{$days}" class="sb makeOrder">{$_modx->lexicon('lf_to_order')}</a>
    </div>
</div>