<!DOCTYPE html>
<html lang="[[++cultureKey]]">
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# [[*class_key:is=`Ticket`:then=`article: http://ogp.me/ns/article#`:else=``]]">
        <meta charset="utf-8" />

        <title>[[*longtitle:htmlent:default=`[[*pagetitle:htmlent]] [[%lf_site_name:htmlent]]`]]</title>
        <meta name="description" content="[[*description:htmlent:default=`[[%lf_description:htmlent]]`]]" />
        <base href="[[++site_url]]" />

        <!-- Controlling DNS prefetching-->
        <meta http-equiv="x-dns-prefetch-control" content="on">
        <link rel="dns-prefetch" href="http://code.jquery.com" />
        <link rel="dns-prefetch" href="http://fonts.googleapis.com" />
        <link rel="dns-prefetch" href="http://www.google-analytics.com" />

        <link rel="author" href="humans.txt" />
        <link rel="icon" href="[[++basetheme.design_url]]images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="[[++basetheme.design_url]]images/favicon.ico" type="image/x-icon" />

        <link rel="stylesheet" type="text/css" href="[[++basetheme.design_url]]css/page.css"/>
        <link rel="stylesheet" type="text/css" data-template-style="true" href="[[++basetheme.design_url]]css/template.css"/>
        <link rel="stylesheet" type="text/css" href="[[++basetheme.design_url]]css/style.css"/>
        <link rel="stylesheet" type="text/css" href="[[++basetheme.design_url]]js/jquery/plugins/bxslider/jquery.bxslider.css"/>
        <link rel="stylesheet" type="text/css" href="[[++basetheme.design_url]]js/jquery/plugins/fancybox/jquery.fancybox.css"/>



        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800,300,600&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

        <script type="text/javascript" language="javascript" src="[[++basetheme.design_url]]js/jquery/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="[[++basetheme.design_url]]js/jquery/plugins/bxslider/jquery.bxslider.js"></script>
        <script type="text/javascript" language="javascript" src="[[++basetheme.design_url]]js/jquery/plugins/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" language="javascript" src="[[++basetheme.design_url]]js/jquery/plugins/jquery.icheck.js"></script>
        <script type="text/javascript" language="javascript" src="[[++basetheme.design_url]]js/scripts.js"></script>
        <script type="text/javascript" language="javascript" src="[[++basetheme.design_url]]js/tabulous.js"></script>
        <script type="text/javascript" language="javascript" src="[[++basetheme.design_url]]js/tabulous.js"></script>

        [[-Molt?
            &cacheFolder=`[[++basetheme.design_url]]min/`
            &jsSources=`
                [[++basetheme.design_url]]js/jquery/plugins/jquery.form.js
                ,[[++basetheme.design_url]]js/jquery/plugins/bxslider/jquery.bxslider.js
                ,[[++basetheme.design_url]]js/jquery/plugins/fancybox/jquery.fancybox.js
                ,[[++basetheme.design_url]]js/jquery/plugins/jquery.icheck.js
                ,[[++basetheme.design_url]]js/scripts.js
                ,[[++basetheme.design_url]]js/tabulous.js
            `
            &cssSources=`
				[[++basetheme.design_url]]css/style.css
				,[[++basetheme.design_url]]js/jquery/plugins/bxslider/jquery.bxslider.css
				[[++basetheme.design_url]]js/jquery/plugins/fancybox/jquery.fancybox.css
            `
            &styleHeadSources=`
                [[++basetheme.design_url]]css/page.css
                ,[[++basetheme.design_url]]css/template.css
            `
            &jquery=`//code.jquery.com/jquery-1.11.3.min.js`
        ]]
        
        <!--[if lt IE 9]>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script>
            var designUrl = '[[++basetheme.design_url]]';
        </script>

        <script>
            var global = {
                siteUrl: '[[++site_url]]',
                baseUrl: '[[++base_url]]',
                assetsUrl: '[[++assets_url]]',
                cultureKey: '[[++cultureKey]]'
            }
        </script>
    </head>
    <body itemscope itemtype="http://schema.org/[[*class_key:is=`Ticket`:then=`Article`:else=`WebSite`]]">
        <!-- Social: Google+ / Schema.org  -->
        <meta itemprop="name" content="[[*longtitle:htmlent:default=`[[*pagetitle:htmlent]] [[%lf_site_name:htmlent]]`]]">
        <meta itemprop="description" content="[[*description:htmlent:default=`[[%lf_description:htmlent]]`]]">
        <link itemprop="image" content="[[*img:notempty=`[[*img:phpthumbof=`w=426`]]`:default=`[[++basetheme.design_url]]images/iconsite/social-image.png`]]">