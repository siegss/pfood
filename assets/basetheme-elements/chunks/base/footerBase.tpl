<div class="footer">
    <div class="cont">
        <div class="links1">
            {$_modx->runSnippet('pdoResources',[
            'parents' => 0,
            'depth' => 0,
            'sortby' => ['menuindex' => 'ASC'],
            'resources' => $.php.implode([$_modx->config['site_start'],$_modx->config['basetheme.id_reviews'],$_modx->config['basetheme.id_how_we_work']],',')
            'tpl' => '@INLINE <div><a href="{$link}">{$pagetitle}</a></div>'
            ])}
            <div>
                {$_modx->lexicon('lf_our_details')}<br/>
                {$_modx->lexicon('lf_ogrn')} {$_modx->lexicon('lf_ogrn_value')}<br/>
                {$_modx->lexicon('lf_inn')} {$_modx->lexicon('lf_inn_value')}
            </div>
        </div>
        <div class="links2">
            <div>{$_modx->lexicon('lf_about_company')}</div>
            {$_modx->runSnippet('pdoResources',[
            'parents' => 0,
            'depth' => 0,
            'sortby' => ['menuindex' => 'ASC'],
            'resources' => $.php.implode([$_modx->config['site_start'],$_modx->config['basetheme.id_contacts'],$_modx->config['basetheme.id_how_we_work'],$_modx->config['basetheme.id_about']],',')
            'tpl' => '@INLINE <div><a href="{$link}">{$menutitle ?: $pagetitle}</a></div>'
            ])}
        </div>
        <div class="links3">
            <div>{$_modx->lexicon('lf_programs')}</div>
            {$_modx->runSnippet('pdoResources',[
            'parents' => $_modx->config['basetheme.id_nutrition_programs'],
            'depth' => 0,
            'sortby' => ['menuindex' => 'ASC'],
            'tpl' => '@INLINE <div><a href="{$link}">{$pagetitle}</a></div>'
            ])}
        </div>
        <div class="contacts">
            <div>{$_modx->lexicon('lf_reference_service')}</div>
            <div class="phone">{$_modx->lexicon('lf_phone_1')}</div>
            <div class="phone">{$_modx->lexicon('lf_phone_2')}</div>
            <br/>
            <div>{$_modx->lexicon('lf_join_us')}</div>
            {$_modx->runSnippet('!getMigxItems',[
            'tvname' => 'gallery',
            'docid' => $_modx->config['basetheme.id_contacts'],
            'tpl' => '@CODE: <a class="sc_ico [[+title]]" href="[[+textarea]]"></a> &nbsp;',
            'limit' => 4
            ])}
        </div>
        <div class="clear"></div>
    </div>
</div>


<div id="feedback"><!-- hidden inline form -->
    <div class="size_modal">
        <div id="formTitleBox">
            <h2>{$_modx->lexicon('lf_fill_form')}</h2>
            <h3 id="contactFormTitle">{$_modx->lexicon('lf_to_get_free_consultation')}</h3>
        </div>
        <form id="formContacts" name="contact"
              action="{$_modx->makeUrl($_modx->config['basetheme.id_ajax_form_contacts'])}" method="post">
            <input type="hidden" name="form" value="f1"/>
            <input type="text" id="f_name" name="name" placeholder="{$_modx->lexicon('lf_your_name')}" required>
            <input type="text" id="f_email" name="email" placeholder="{$_modx->lexicon('lf_your_email')}">
            <input type="text" id="f_phone" name="phone" placeholder="{$_modx->lexicon('lf_your_phone')}" required>
            <input type="hidden" id="f_product" name="product">
            <input type="hidden" id="f_product_name" name="product_name">
            <input type="hidden" id="f_days" name="days">
            <input type="hidden" id="f_price" name="price">
            <div class="err"></div>
            <button type="submit">{$_modx->lexicon('lf_contact_send')}</button>
        </form>
        <h3 id="formContacts_SuccessMessage" class="formMessage"
            style="display: none">{$_modx->lexicon('lf_contact_successMessage')}</h3>
        <h3 id="formContacts_ErrorMessage" style="display: none">{$_modx->lexicon('lf_contact_errorMessage')}</h3>
    </div>
</div>

<div id="orderFormWrapper" style="display: none"><!-- hidden inline form -->
    <div class="size_modal">
        {$_modx->runSnippet('!msOrder',[
            'tplOuter' => 'orderOuterCommon',
            'tplEmpty' => 'orderOuterCommon'
        ])}
    </div>
</div>


<div id="hide-layout" class="hide-layout"></div>

<script src="{$_modx->config['basetheme.design_url']}js/jquery/plugins/validation/jquery.validate.js"></script>
<script src="{$_modx->config['basetheme.design_url']}js/jquery/plugins/validation/localization/messages_ru.js"></script>
<script src="{$_modx->config['basetheme.design_url']}js/jquery/plugins/jquery.form.js" type="text/javascript"
        language="javascript"></script>
<script src="{$_modx->config['basetheme.design_url']}js/app/lib/site.js"></script>
<script src="{$_modx->config['basetheme.design_url']}js/app/lib/siteMode.js"></script>
<script src="{$_modx->config['basetheme.design_url']}js/app/mode/themeMode.js"></script>
<script src="{$_modx->config['basetheme.design_url']}js/app/modules/formValidate.js"></script>
<script src="{$_modx->config['basetheme.design_url']}js/app/modules/formAjax.js"></script>
<script src="{$_modx->config['basetheme.design_url']}js/init.js"></script>
</body>
</html>