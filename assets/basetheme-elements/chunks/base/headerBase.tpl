<div class="header">
    <div class="header_size">
        <a class="logo" href="{$_modx->config['site_url']}"><img
                    src="{$_modx->config['basetheme.design_url']}images/logony.png"/></a>
        <div class="phones">
            <div>{$_modx->lexicon('lf_phone_1')}</div>
            <div>{$_modx->lexicon('lf_phone_2')}</div>
            <div><a href="#feedback" rel="nofollow" class="modalbox">{$_modx->lexicon('lf_order_consultation')}</a></div>
        </div>

        <div class="sl_tog" onclick="$('.top_main ul').slideToggle();"></div>
        <br clear="both"/>
    </div>


    <div class="top_main">
        <center>
            {$_modx->runSnippet('pdoMenu@mainMenu',[
            'where' => ['class_key:NOT LIKE'=> 'Ticket'],
            'includeTVs' => 'ico',
            'processTVs' => 1,
            'tvPrefix' => '',
            'hereClass' => 'act',
            'tpl' => '@INLINE <li class="ico"></li><li {$classes}><a href="{$link}">{$pagetitle}</a></li>',
            'tplParentRow' => 'mainMenuParentRowCommon',
            'tplInner' => 'mainMenuInnerCommon',
            'tplInnerRow' => 'mainMenuInnerRowCommon'
            ])}
        </center>
    </div>
    <br clear="both"/>

</div>