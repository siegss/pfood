<?php
define('MODX_CONNECTOR_INCLUDED', 1);
require_once dirname(dirname(__FILE__)).'/index.php';
$path = $modx->getOption('core_path') .'components/lexiconfrontend/processors/';

$modx->request->handleRequest(array(
    'processors_path' => $path,
    'location' => '',
));
