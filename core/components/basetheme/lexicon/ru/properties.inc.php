<?php

/*
 * Molt
 */
$_lang['basetheme_molt_prop_tpl'] = 'Шаблон с базовыми переменными JS.';
$_lang['basetheme_molt_prop_cssFilename'] = 'Базовое имя готового CSS файла.';
$_lang['basetheme_molt_prop_cssSources'] = 'Список через запятую CSS файлов для обработки.';
$_lang['basetheme_molt_prop_styleHeadSources'] = 'Список через запятую CSS файлов для обработки и вставки в секцию head как элемент style.';
$_lang['basetheme_molt_prop_cssPack'] = 'Включить минификацию CSS?';
$_lang['basetheme_molt_prop_styleHeadPack'] = 'Включить минификацию CSS для секции head?';
$_lang['basetheme_molt_prop_cssDeferred'] = 'Отложенная загрузка CSS файла, после загрузки страницы.';
$_lang['basetheme_molt_prop_cssPlaceholder'] = 'Имя плейсхолдера css. Используется,если &cssRegister=`placeholder`';
$_lang['basetheme_molt_prop_styleHeadPlaceholder'] = 'Имя плейсхолдера css. Используется,если &styleHeadRegister=`placeholder`';
$_lang['basetheme_molt_prop_cssRegister'] = 'Подключение сss: можно сохранить в плейсхолдер (placeholder) или вызвать в теге "head" (default).';
$_lang['basetheme_molt_prop_styleHeadRegister'] = 'Подключение сss: можно сохранить в плейсхолдер (placeholder) или вызвать в теге "head" (default).';
$_lang['basetheme_molt_prop_jsFilename'] = 'Базовое имя готового JS файла.';
$_lang['basetheme_molt_prop_jsSources'] = 'Список через запятую JS файлов для обработки.';
$_lang['basetheme_molt_prop_jsPack'] = 'Включить минификацию JS?';
$_lang['basetheme_molt_prop_jsDeferred'] = 'Отложенная загрузка JS файла, после загрузки страницы.';
$_lang['basetheme_molt_prop_jsPlaceholder'] = 'Имя плейсхолдера javascript. Используется, если &registerJs=`placeholder`';
$_lang['basetheme_molt_prop_jsRegister'] = 'Подключение javascript: можно сохранить в плейсхолдер (placeholder), вызвать в теге "head" (startup) или разместить перед закрывающим "body" (default).';
$_lang['basetheme_molt_prop_incjQuery'] = 'Отдельно подключить библиотеку jQuery из CDN.';