<?php

$_lang['basetheme_molt_prop_tpl'] = 'Template with basic variables JS.';
$_lang['basetheme_molt_prop_cssFilename'] = 'Base name for the CSS file.';
$_lang['basetheme_molt_prop_cssSources'] = 'List of comma-separated CSS files to be processed.';
$_lang['basetheme_molt_prop_styleHeadSources'] = 'List of comma-separated CSS files to be processed and include in section head how tag style.';
$_lang['basetheme_molt_prop_cssPack'] = 'Enable Minification CSS?';
$_lang['basetheme_molt_prop_styleHeadPack'] = 'Enable Minification CSS for section head?';
$_lang['basetheme_molt_prop_cssDeferred'] = 'Lazy loading CSS file after loading the page.';
$_lang['basetheme_molt_prop_cssPlaceholder'] = 'Name placeholder css. Used if &cssRegister=`placeholder`';
$_lang['basetheme_molt_prop_styleHeadPlaceholder'] = 'Name placeholder css. Used if &styleHeadRegister=`placeholder`';
$_lang['basetheme_molt_prop_cssRegister'] = 'Connection CSS : You can save the placeholder (placeholder) or cause the tag "head" (default).';
$_lang['basetheme_molt_prop_styleHeadRegister'] = 'Connection CSS : You can save the placeholder (placeholder) or cause the tag "head" (default).';
$_lang['basetheme_molt_prop_jsFilename'] = 'Base name for the JS file.';
$_lang['basetheme_molt_prop_jsSources'] = 'List  of comma-separated JS files to be processed.';
$_lang['basetheme_molt_prop_jsPack'] = 'Enable Minification JS?';
$_lang['basetheme_molt_prop_jsDeferred'] = 'Lazy loading JS file after loading the page.';
$_lang['basetheme_molt_prop_jsPlaceholder'] = 'Name placeholder javascript. Used if &registerJs=`placeholder`';
$_lang['basetheme_molt_prop_jsRegister'] = 'Connection javascript: You can save the placeholder (placeholder), cause the tag "head" (startup) or put before the closing "body" (default).';
$_lang['basetheme_molt_prop_incjQuery'] = 'Separately connect the jQuery library from the CDN.';